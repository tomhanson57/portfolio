"use strict"
module.exports = {
    /***************************************
     ** Drag and Drop Image
     ***************************************/
    initInstagram: function () {

        if ($('.main-content').length) {

            var Instafeed = require("instafeed");

            // var loadMoreButton = document.getElementById("loadMoreButton");
            var instafeed = new Instafeed({
                accessToken: "3176159592.1677ed0.7d90126db397447bbad0808f6e389a74",
                // sortBy: 'most-liked',
                limit:4,
                imageResolution: "low-resolution",
                imageTemplate: "",
                // carouselImageTemplate: "<div class=\"text-center\"><img src=\"{{image}}\" width=\"270\" height=\"270\"></div>",

                onSuccess: function(response){
                    $("#instafeed").html("");
                    response.data.forEach(function(e){
                        var imgUrl = e.images.standard_resolution.url;
                        var caption = e.caption.text;
                        var fullName = e.user.full_name;
                        var profilePicture = e.user.profile_picture;
                        var link = e.link;

                        var html =

                        '<div class="col-lg-3 col-xl-3 col-md-3 col-sm-6 col-xs-6">' +
                        '                <div class="row">' +
                        '                    <div class="col-12">' +
                        '                        <a href="'+ link +'" TARGET="_blank">' +
                        '                        <div class="social-img" style="background: url('+imgUrl+') no-repeat center center / cover;" ></div>' +
                        '                        </a>' +
                        '                        <div class=" d-flex justify-content-between pt-3 pr-5">' +
                        '                            <div class="d-flex">' +
                        '                                <div class="social-profile rounded-circle" style="background: url('+profilePicture+') no-repeat center center / cover;"></div>' +
                        '                                <div class="ml-2"><i class="fa fa-instagram"><span class="font-weight-bold"> '+ fullName +'</span></i></div>' +
                        '                            </div>' +
                        '                            <div class="justify-content-end">' +
                        '                                <div><p class="social-time"></p></div>' +
                        '                            </div>' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </div>' +
                        '                <div class="row">' +
                        '                    <div class="col-12 text-left">' +
                        '                        <p>'+ caption +'</p>' +
                        '                    </div>' +
                        '                </div>' +
                        '            </div>';

                        $("#instafeed").append(html);

                        // e.images.standard_resolution = {
                        //     url: e.images.standard_resolution.url.replace('320x320', '270x270'),
                        //     width: 270,
                        //     height: 270
                        // };
                    });

                    return true;
                },

                onError: function(message) {
                    console.log(message);
                }
            });

            // loadMoreButton.addEventListener("click", function() {
            //     instafeed.next();
            // });

            instafeed.run();

            $.fn.extend({
                matchHeight: function(data){
                    var maxHeight = 0;
                    $(this).each(function() {
                        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
                    });
                    $(this).height(maxHeight);
                }
            });
        }
    },

};