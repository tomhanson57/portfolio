"use strict"
module.exports = {
    /***************************************
     ** Click and return to Top
     ***************************************/
    toTop: function (btnId) {

        // $('body').append('<img id="toTop" src="'+siteUrl+'assets/img/icon/icon-top.svg">');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1000) {
                $(btnId).fadeIn();
            } else {
                $(btnId).fadeOut();
            }
        });
        $(btnId).click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
    },
    /***************************************
     ** Input Card Number - Change Format
     ***************************************/
    inputCardNumber: function(inputId) {
        $(inputId).on('input',function(e){
            var cardNumber = $(this).val().replace(/ /g, '').split('');
            var format = "";

            $(this).attr('maxlength', 19);
            for(var i=0;i<cardNumber.length;i++){
                format += cardNumber[i];
                if((i+1)%4 === 0 && i !== 0 && i !== cardNumber.length-1){

                    format += ' ';
                }
            }
            $(this).val(format);

        });
    }


};