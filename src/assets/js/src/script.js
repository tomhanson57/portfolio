
/*
|--------------------------------------------------------------------------
| Custom Javascript code
|--------------------------------------------------------------------------
|
| Now that you configured your website, you can write additional Javascript
| code inside the following function. You might want to add more plugins and
| initialize them in this file.
|
*/

$(function() {

$(document).mouseup(function(e)
{
    var container = $(".cart-popup");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        if(container.hasClass("show")){

            container.removeClass("show");
        }
    }
});


$(document).ready(function(){
    var addOnPrice = 3;
    instagram.initInstagram();
    FE.toTop('#toTop');
    FE.inputCardNumber("#fake-cardnumber");

    $('#searchPhrase').change(function (e) {
        e.preventDefault();
        var searchQuery = "?q=" + $("#searchPhrase").val().split(" ").join("+");
        var url = siteUrl + "search" + searchQuery;
        window.location = url;

    });

    $('select[name="variantBox"]').change(function(){
        var price = $(this).find(':selected').attr('data-price');
        $('#proPrice').html(price);

        var purchasableId = $(this).find(':selected').val();
        $('input[name="purchasableId"]').val(purchasableId);

        if($('#variant-'+purchasableId).length > 0){
            $('#pro-d-des').html("").html($('#variant-'+purchasableId).val());
        }

    });

    $('input[name="options[cake-sign]"]').change(function(){

        if($(this).val() === "No"){
            var price = $(this).attr("data-value");
            $('#words-to-say').attr('maxlength', 0);
            $('#proPrice').html(formatCurrency(price));

        }else if($(this).val() === "Happy Birthday"){
            var price = $("#add-on-no").attr("data-value");
            $('#words-to-say').attr('maxlength', 0);
            $('#proPrice').html(formatCurrency(price*1 + $(this).attr("data-value")*1 ));

        }else{
            var price = $("#add-on-no").attr("data-value");
            $('#words-to-say').removeAttr('maxlength');
            $('#proPrice').html(formatCurrency(price*1 + $(this).attr("data-value")*1 ));

        }
    });

    /** Change Quantity in Cart Page */
    $('.cart-quantity').on('input',function(e) {

        if (!$(this).val().trim()) {
            $(this).val(1);
        }


        var index = $(this).attr('data-value');
        var $form = $('.form-cart-update-' + index);
        var form_data = $form.serializeArray();

        ajaxUpdateCart(form_data);
    });

    /** Delete Item in Cart Page */
    $('.cart-delete-button').click(function(){
        var index = $(this).attr('data-value');
        var $form = $('.form-cart-delete-'+index);
        var form_data = $form.serializeArray();

        ajaxUpdateCart(form_data);
    });

    /** Add to Cart */
    $('.add-to-cart').click(function(){

        $('.add-to-cart').text('ADDING...');

        //Update Cart Item
        var form_data = $('#form-purchase-product').serializeArray();
        ajaxUpdateCart(form_data);
    });

    /** Apply Coupon */
    $('#btn-coupon').click(function(){
        $('#couponForm').submit();
    })

    /****
     * Functions
     * @param form_data
     */
    function ajaxUpdateCart(form_data){
        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': form_data,
            'url': 'commerce/cart/update-cart',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            // log(response.cart);

            //Add transaction fee only if there is item in the cart
            if(!response.cart.adjustments.shipping && Object.keys(response.cart.lineItems).length > 0 ){
                //Update Shipping Base 0.3
                var $form = $('#daciDelivery');
                var form_data = $form.serializeArray();
                ajaxUpdateCart(form_data);

            }else if(response.cart.adjustments.shipping && response.cart.adjustments.shipping[0].sourceSnapshot.methodId !==1 && Object.keys(response.cart.lineItems).length === 0){
                var $form = $('#daciDelivery');
                var form_data = $form.serializeArray();
                for(var i=0;i<form_data.length;i++){
                    if(form_data[i].name === "shippingMethodHandle"){
                        form_data[i].value = "freeShipping";
                    }
                }

                ajaxUpdateCart(form_data);
            }

            reloadCartValue(response.cart);


        }).fail(function (error) {
            console.log("AJAX UPDATE CART ERROR:");
            console.log(error);
        });
    }

    function reloadCartValue(cart){
        if(!cart)
        {
            window.location.reload();
            return;
        }

        var lineItemIds = [];
        $.each(cart.lineItems, function(key, value){
            lineItemIds.push(value.id);
            //update cart-popup value
            $('.cart-pop-content').find('div[data-value="'+value.id+'"]').find('.cart-pop-qty').html(value.qty);
            $('.cart-pop-content').find('div[data-value="'+value.id+'"]').find('.cart-pop-price').html(formatCurrency(value.salePrice));
            //update cart-page table row
            $('#priceLineItem'+key).html(formatCurrency(value.subtotal));
        });

        $('.add-to-cart').off("click");
        $('.add-to-cart').attr('href', siteUrl + 'checkout/cart').text('VIEW CART').removeClass('add-to-cart');



        $('.cart-badge-number').html(lineItemIds.length);
        //update cart-popup-content
        $('#cart-pop-content').load(location.href + " #cart-pop-content");

        //update cart page

        $('#itemSubTotal').html(formatCurrency(cart.itemSubtotal));
        //tax is included in the price
        $('#taxValue').html(formatCurrency(0));
        $('#taxIncValue').html(formatCurrency(0));


        var transactionFee = 0;
        if(cart.adjustments.shipping)
            for(var i=0;i<cart.adjustments.shipping.length;i++){
                transactionFee += cart.adjustments.shipping[i].amount;
            }
        if(transactionFee === 0.3){
            $('#transactionFee').html(formatCurrency(0));
            $('#totalPrice').html(formatCurrency(0));
        }else{
            $('#transactionFee').html(formatCurrency(transactionFee));
            $('#totalPrice').html(formatCurrency(cart.totalPrice));
        }


        removeDeletedRow(lineItemIds);

    }

    function removeDeletedRow(lineItemIds){
        $('.table-cart').find('tr:not(:last)').each(function(){
            var id = $(this).find('input[name="lineItemId"]').val();
            if(lineItemIds.indexOf(id) === -1){
                $(this).fadeOut().remove();
            }
        });

        $('.row-line-items').each(function(){
            var id = $(this).attr('data-value');
            if(lineItemIds.indexOf(id) === -1){
                $(this).remove();
            }
        });


    }

    function formatCurrency(price){
        var neg = false;
        if(price < 0) {
            neg = true;
            price = Math.abs(price);
        }
        return (neg ? "-$" : '$') + parseFloat(price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    }

    function log(data){
        console.log(data);
    }


});


});

