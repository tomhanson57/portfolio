
/*
|--------------------------------------------------------------------------
| Commerce Javascript code
|--------------------------------------------------------------------------
|
| Now that you configured your website, you can write additional Javascript
| code inside the following function. You might want to add more plugins and
| initialize them in this file.
|
*/

$(function() {

    console.log("javascript loaded");
    $(".brands-checkbox").find("input").each(function () {
        var $this = $(this);
        $this.change(function () {
            priceMin = priceMinRange;
            priceMax = priceMaxRange;
            sizeArray = [];
            colorArray = [];
            if ($this.is(":checked")) {
                touggleAllAndRest($(".brands-checkbox"), $this, "all-brand", brandArray);
                if (typeof($this.attr("data-value")) !== "undefined") {
                    brandArray.push($this.attr("data-value"));
                }
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            } else {
                brandArray.splice(brandArray.indexOf($this.attr("data-value")), 1);
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            }
        });
    });

    $(".color-checkbox").find("input").each(function () {
        var $this = $(this);
        $this.change(function () {
            priceMin = priceMinRange;
            priceMax = priceMaxRange;
            if ($this.is(":checked")) {
                touggleAllAndRest($(".color-checkbox"), $this, "all-color", colorArray);
                if (typeof($this.attr("data-value")) !== "undefined") {
                    colorArray.push($this.attr("data-value"));
                }
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            } else {
                colorArray.splice(colorArray.indexOf($this.attr("data-value")), 1);
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            }
        });
    });

    $(".type-checkbox").find("input").each(function () {
        var $this = $(this);
        $this.change(function () {
            priceMin = priceMinRange;
            priceMax = priceMaxRange;
            brandArray = [];
            sizeArray = [];
            colorArray = [];
            if ($this.is(":checked")) {
                touggleAllAndRest($(".type-checkbox"), $this, "all-type", typeArray);
                if (typeof($this.attr("data-value")) !== "undefined") {
                    typeArray.push($this.attr("data-value"));
                }
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            } else {
                typeArray.splice(typeArray.indexOf($this.attr("data-value")), 1);
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            }
        });
    });

    /**
     *  add class choosed to every clicked a link under class sizes
     */
    $(".sizes").find("a").each(function () {
        $(this).click(function () {
            priceMin = priceMinRange;
            priceMax = priceMaxRange;
            $(this).toggleClass("choosed");
            if ($(this).hasClass("choosed")) {
                if ($(this).attr('id') === "all-size") {
                    $(".sizes").find("a").each(function () {
                        if ($(this).attr('id') !== "all-size") {
                            $(this).removeClass("choosed");
                            sizeArray.splice(sizeArray.indexOf($(this).html()), 1);
                        }
                    });
                } else {
                    var all = $("#all-size");
                    all.removeClass("choosed");
                    if (jQuery.inArray(all.attr("data-value"), sizeArray) !== -1) {
                        sizeArray.splice(sizeArray.indexOf(all.attr("data-value")), 1);
                    }
                }
                if (typeof($(this).attr("data-value")) !== "undefined") {
                    sizeArray.push($(this).attr("data-value"));
                }
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            } else {
                sizeArray.splice(sizeArray.indexOf($(this).html()), 1);
                window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
            }
        });
    });

    $("#sortByOption").change(function () {
        window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
    });

    $(".noUi-handle-lower").bind("mouseup touchend",function () {
        priceMin = $("#price-min").html().substr(1);
        window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
    }).mouseout(function () {
        priceMin = $("#price-min").html().substr(1);
        window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
    });

    $(".noUi-handle-upper").bind("mouseup touchend",function () {
        priceMax = $("#price-max").html().substr(1);
        window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
    }).mouseout(function () {
        priceMax = $("#price-max").html().substr(1);
        window.location.href = constructLink(brandArray, sizeArray, typeArray, colorArray);
    });

    /**
     * function to use to toggle "all" element and the rest
     * when all selected , remove others selected attributes
     * when other selected, unselect all
     * @param Parent object parent object where everything happens
     * @param object object the object group you want process
     * @param idName string id name of "all" element
     * @param array array the array you store the value
     */

    function touggleAllAndRest(Parent, object, idName, array) {
        if (object.attr('id') === idName) {
            Parent.find("input").each(function () {
                if ($(this).attr('id') !== idName) {
                    $(this).prop('checked', false);
                    array.splice(array.indexOf(object.attr("data-value")), 1);
                }
            });
        } else {
            var all = $("#" + idName);
            all.prop('checked', false);
            if (jQuery.inArray(all.attr("data-value"), array) !== -1) {
                array.splice(array.indexOf(all.attr("data-value")), 1);
            }
        }
    }

    function constructLink(brandArray, sizeArray, typeArray, colorArray) {
        var sort = $("#sortByOption").val();
        var link = siteUrl + 'shop';
        link = typeArray.length > 0 ? link + "/" + arrayToUrl("", typeArray) : link;
        link = brandArray.length > 0 ? link + "/" + arrayToUrl("", brandArray) : link;
        link = colorArray.length > 0 ? link + "?" + arrayToUrl("color=", colorArray) : link +"?";
        link = sizeArray.length > 0 ? link + "&" + arrayToUrl("size=", sizeArray) : link;
        link = priceMin === priceMinRange ? link : link + "&priceMin=" + priceMin;
        link = priceMax === priceMaxRange ? link : link + "&priceMax=" + priceMax;
        link = sort === "" ? link : link + "&sort=" + sort;
        return link;
    }

    /**
     * construct array make it ready to put in url
     * @param name
     * @param array
     * @returns {Blob|ArrayBuffer|Array.<T>|*}
     */
    function arrayToUrl(name, array) {
        var string = name;
        $.each(array, function (index, value) {
            string = string + value + "_";
        });
        return string.slice(0, -1);
    }

    $('.product-list-page-link').each(function(){
        $(this).click(function(){
            var pageLink = $(this).attr('data-value');
            var paramLink = constructLink(brandArray, sizeArray, typeArray, colorArray).split("?").pop();
            var Link = pageLink + "?" + paramLink;
            window.location.href = Link;
        });
    });

    /**
     *  checkout/delivery
     */
    $('.transportation').find('.charges').each(function () {
        $(this).click(function () {
            $(this).toggleClass("selected-delivery");
            var selectedMethods = $(this).attr('data-value');
            if ($(this).hasClass("selected-delivery")) {
                $('#shippingMethodInput').val(selectedMethods);
                $('.transportation').find('.charges').each(function () {
                    if (selectedMethods !== $(this).attr('data-value')) {
                        $(this).removeClass("selected-delivery");
                    }
                });
            } else {
                $('#shippingMethodInput').val("");
            }
        });
    });


    $('#contactFormSubmit').click(function () {
        $('#deliveryContactForm').submit();
    });


    $('#deliveryContactForm').submit(function (e) {
        var name = $("#name");
        var msg = $("#message");
        var response = grecaptcha.getResponse();
        if (name.hasClass("error")) {
            name.removeClass("error");
        }
        if (msg.hasClass("error")) {
            msg.removeClass("error");
        }
        if (name.val() === "") {
            name.addClass('error');
            e.preventDefault();
        }
        else if (msg.val() === "") {
            msg.addClass('error');
            e.preventDefault();
        }
        else if (response.length === 0) {
            $('#recaptcha-error-msg').html('Recaptcha is not verified.');
            e.preventDefault();
        } else {
            //clear error message
            document.getElementById("email-error-msg").innerHTML = '';
            $('#recaptcha-error-msg').html('');
        }
    });
    /**
     * checkout/shipping
     */
    $('#shippingFormSubmit').click(function () {
        if ($('#shippingMethodInput').val() === "") {
            $('.transportation').find('.error-message').html("Please Select a Shipping Method");
        } else {
            $('#shippingMethodForm').submit();
        }
    });

    /**
     * checkout/cart
     */
    $('.cart-delete-button').click(function () {
        //Round is a count interval in the loop of cart.lineItems, it is helping locate the specific form in the loop to submit
        console.log("form clicked");
        if (typeof($(this).attr("data-value")) !== 'undefined') {
            var Round = $(this).attr("data-value");
            $(".form-cart-delete-" + Round).submit();
        }
    });

    $('.cart-update-quantity').on('change', function () {
        console.log("form clicked");
        if (typeof($(this).attr("data-value")) !== 'undefined') {
            var Round = $(this).attr("data-value");
            $(".form-cart-update-" + Round).submit();
        }
    });
    
    /** Cart dropdown click */
    $('.cart-dropdwon-click').click(function(){
        window.location = $(this).attr('data-href');
    })

    /**
     * checkout/payment
     */

    $('#paymentSubmit').click(function(){
        $('#paymentForm').submit();
    });

    /**
     * checkout progress bar
     */
    $('.link-disable').find("a").each(function(){
        $(this).click(function(e){
            e.preventDefault();
        });
    });

    /**
     * widget search box
     */

    $('#searchPhrase').change(function () {
        var searchQuery = "?q=" + $("#searchPhrase").val().replace(" ", "+");
        window.location.href = siteUrl + "search" + searchQuery;
    });


});
